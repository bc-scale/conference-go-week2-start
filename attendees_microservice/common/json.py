from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}  # static property, NOT A VARIABLE

    def default(self, o):  # o = object, Name is seen in djano rabbit hole
        if isinstance(o, self.model):
            # check to see if the a property on model
            d = {}
            if hasattr(o, "get_api_url"):
                # json response only reads in strings, is auto convert
                # The hasattr() function returns True if the specified
                # object has the specified attribute, otherwise False.
                d["href"] = o.get_api_url()
                # get a specific url becasue of kwargs={"pk": self.pk}
            for property in self.properties:
                value = getattr(o, property)
                # The getattr() function returns the value of the specified
                # attribute from the specified object. Therefore, returning
                # specific property on what model using encoders
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            # define get_extra_data(), which should return a dict
            # of JSON serizalizable values
            return d
        else:
            return super().default(o)
            # The super() function is used to give access to methods
            # and properties of a parent or sibling class.

    def get_extra_data(self, o):
        return {}
